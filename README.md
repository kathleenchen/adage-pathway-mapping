# Dependencies
Install pandas by running:
	
	pip install pandas

# Shell Script (determine significant pathways)
## multiple_models_pathway_cons.sh
The constants should be updated to fit your filepaths.

Also note that to actually name the resulting significant pathways files, I make the assumption that the individual ADAGE output files are named using this convention:
	
	all-pseudomonas-gene-normalized_300_batch10_epoch500_corrupt0.1_lr0.01_seed1_1_seed2_1_network_SdA.txt

and grab the number between the two underscores accordingly. 
	
	./multiple_models_pathway_cons.sh

# Python Script (network construction)
## multi_lists_preprocessing.py
After you've output your significant pathways for each model into a directory, you should be able to run this script with the path to that directory and an output filename specified. 

	python multi_list_preprocessing.py ./pw_coverage_individual 1000final_network.txt




