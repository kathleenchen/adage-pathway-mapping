'''

This script is used to construct the pathway network from multiple lists
of significant pathways (from multiple ADAGE models).
It takes in two arguments:
   
   python multi_lists_preprocessing.py -sig pathways directory- -output file-
   
   where the significant pathways directory contains outputs from multiple
   calls of pathway_coverage.R. The output file is the name of the file this
   script should write to. One file is created with the network for the
   combined significant pathway data over multiple ADAGE models.

'''

import os
import pandas as pd
import sys
import make_network_pathways as mnp


def mergeNetworkDicts(original_dict, to_merge):
	for key, value in to_merge.iteritems():
		if key not in original_dict:
			original_dict[key] = value
		else:
			original_dict[key] = original_dict[key] + value
	return original_dict

folder_path = sys.argv[1]
output_all_rel_file = sys.argv[2]

final_network = None
for fn in os.listdir(folder_path):
	path_to_file = folder_path + "/" + fn
	(same, inv) = mnp.networkConstruction(path_to_file)
	
	if final_network is None:
		final_network = (same, inv)
	else:
		fn_same = final_network[0]
		fn_inv = final_network[1]
		updated_same = mergeNetworkDicts(fn_same, same)
		updated_inv = mergeNetworkDicts(fn_inv, inv)
		final_network = (updated_same, updated_inv)

# specify the threshold for which edges should be dropped. 
pw_network = mnp.postProcessing(final_network[0], final_network[1], 10)
pw_network.to_csv(path_or_buf=output_all_rel_file,
                  sep='\t',
                  index=False)
