"""

Used to create the pathway-pathway graph.
This script takes in two arguments:
   
   python make_network_pathways.py -significant pathways file- -output file-
   
   where the significant pathways file should be the output from
   pathway_coverage.R. The output file is the name of the file this script
   should write to.

"""

import numpy as np
import sys
import itertools
import pandas as pd

def __dictToDataframe(pw_dict, drop_weights_below):
    """ Converts the pathway dictionary into the formatted dataframe
        which will become the output to our CSVs.
    :param pw_dict: pathway relation (key) to list of node #s (value)
    :param drop_weights_below: int, specify the threshold for edge weights
    :returns: dataFrame for the final network file.
    """
    network_df = pd.DataFrame(columns=['PW1', 'PW2',
                                       'Weight', 'Nodes', 'Side'])
    current_idx = 0
    for (pw1, pw2, side), value in pw_dict.iteritems():
        weight = len(value)
        if weight > drop_weights_below:
            nodes = ' '.join(map(str, value))
            network_df.loc[current_idx] = [pw1, pw2, weight, nodes, side]
            current_idx += 1  # easier to append nodes this way
    network_df = network_df.sort(['Side', 'Weight'], ascending=False)
    return network_df


def __edgeKey(pw1, pw2, relation):
    fst = min(pw1, pw2)
    snd = max(pw1, pw2)
    return (fst, snd, relation)


def __updateTwoWayDict(pw_dict, pw):
    if pw not in pw_dict:
        pw_dict[pw] = 1
    else:
        pw_dict[pw] += 1
    return pw_dict


def __vertexDegree(pw_dict, pw):
    count = 0
    for (pw1, pw2, side) in pw_dict.keys():
        if pw == pw1 or pw == pw2:
            count += 1
    return count


def twoWayEdges(same_dict, inv_dict):
    relation = "inv"
    two_way_edge_verts = {}  # no. of two-way edges a vertex (pathway) has
    for (pw1, pw2, side) in same_dict.keys():
        if (pw1, pw2, relation) in inv_dict:
            two_way_edge_verts = __updateTwoWayDict(two_way_edge_verts, pw1)
            two_way_edge_verts = __updateTwoWayDict(two_way_edge_verts, pw2)

    vertices_data = {'two_way_deg': two_way_edge_verts.values(),
                     'total_degree': []}
    
    for pw in two_way_edge_verts.keys():
        degree = __vertexDegree(same_dict, pw) + __vertexDegree(inv_dict, pw)
        degree -= two_way_edge_verts[pw]
        vertices_data['total_degree'].append(degree)

    verts_df = pd.DataFrame(data=vertices_data,
                            index=two_way_edge_verts.keys())

    verts_df['proportion'] = verts_df['two_way_deg']/verts_df['total_degree']
    verts_df = verts_df.sort(['two_way_deg', 'total_degree'], ascending=False)
    return verts_df
            

def networkConstruction(node_pathway_file):
    """ Store the pathway relations that will become our edges.
        (relation types: direct or inverse)
    :param node_pathway_file: path to significant pathways file.
                              This file is created from the
                              pathway_coverage.R script.
    :returns: tuple of the direct relation dictionary and inverse dictionary.
              Both dictionaries have a tuple-of-strings key format
                of (pathway1, pathway2, relation)
              and the corresponding value is an int list of nodes the
              relation was found in.
    """
    # the input file is read into a dataframe
    node_pathway_df = pd.read_table(node_pathway_file, sep='\t', header=0,
                                    usecols=['node', 'side', 'pathway'])

    # pre-process to shorten naming for nodes and pathways
    node_pathway_df['node'] = node_pathway_df['node'].apply(
                                lambda x: int(filter(str.isdigit, x)))
    node_pathway_df['pathway'] = node_pathway_df['pathway'].apply(
                                lambda x: label_trim(x))
    node_pathway_df = node_pathway_df.sort(['node', 'side'])

    # for direct (pos-pos, neg-neg) relationships.
    relation = 'dir'
    pw_network_dict_same = {}
    node_groupings_same_side = node_pathway_df.groupby(['node', 'side'])

    for (node, side), group in node_groupings_same_side:
        # faster to iterate through a list vs. a dataframe
        assoc_pathways = group['pathway'].tolist()
        pairings = list(itertools.combinations(assoc_pathways, 2))

        for pathway_pair in pairings:
            # need to be the same pair of pathways and direct rel.
            pathway_pair = __edgeKey(pathway_pair[0], pathway_pair[1],
                                     relation)
            if (pathway_pair not in pw_network_dict_same):
                pw_network_dict_same[pathway_pair] = [node]
            else:
                pw_network_dict_same[pathway_pair].append(node)

    # for inverse relationships.
    relation = 'inv'  # for uniformity with the same side relation
    pw_network_dict_inv = {}
    node_groupings_either_side = node_pathway_df.groupby(['node'])
    for node, group in node_groupings_either_side:
        divide_by_side = group.groupby(['side'])
        # both pos and neg exist => pairwise inverse rel
        if (len(divide_by_side) == 2):
            pos_in_node = []
            neg_in_node = []
            for side, subgroup in divide_by_side:
                if 'pos' in side:
                    pos_in_node = subgroup['pathway'].tolist()
                else:
                    neg_in_node = subgroup['pathway'].tolist()
            for i in xrange(0, len(pos_in_node)):
                for j in xrange(0, len(neg_in_node)):
                    pathway_pair = __edgeKey(pos_in_node[i], neg_in_node[j],
                                             relation)
                    if (pathway_pair not in pw_network_dict_inv):
                        pw_network_dict_inv[pathway_pair] = [node]
                    else:
                        pw_network_dict_inv[pathway_pair].append(node)

    return (pw_network_dict_same, pw_network_dict_inv)


def label_trim(x):
        to_remove = "- Pseudomonas aeruginosa PAO1"
        x = x.split(' ', 1)[1]
        if to_remove in x:
            return x[0:x.index(to_remove)] + "PA01"
        else:
            return x.strip()


def __networkReport(network_combined_df):
    pw1_set = set(network_combined_df['PW1'])
    pw2_set = set(network_combined_df['PW2'])
    num_pws = str(len(list(pw1_set | pw2_set)))
    print "This network covers " + num_pws + " pathways."


def postProcessing(network_dict_same, network_dict_inv, weight_threshold):
    """ Trim the pathway names, drop edges below a certain weight threshold.
    :param network_dict_same: dictionary for direct relations
    :param network_dict_inv: dictionary for inverse relations
    :param weight_threshold: int, specify threshold for edges in the network
    :returns: dataframe for the final network file.
    """

    network_same_df = __dictToDataframe(network_dict_same, weight_threshold)
    network_inv_df = __dictToDataframe(network_dict_inv, weight_threshold)

    network_combined_df = network_same_df.append(network_inv_df)

    __networkReport(network_combined_df)
    return network_combined_df

if __name__ == '__main__':
    node_pathway_file = sys.argv[1]
    output_all_rel_file = sys.argv[2]

    (same, inv) = networkConstruction(node_pathway_file)
    pw_network = postProcessing(same, inv, 1)

    pw_network.to_csv(path_or_buf=output_all_rel_file,
                      sep='\t',
                      index=False)
