#!/bin/bash

# This script loops through the ADAGE output files in the specified folder and 
# runs pathway_coverage.R on each of these weight matrices. Individual output 
# files are generated, where naming is based on the assumption that the input 
# files follow the naming prefix of PREFIX_RM + the unique model number. 

# Update these constants to your desired filepaths.
CURR_FOLDER="individual_ADAGE"  
RUN_SCRIPT="./data/pathway_coverage.R"  
COMPENDIUM="./data/all-pseudomonas-gene.pcl"  
KEGG_LIST="./data/pseudomonas_KEGG_terms.txt" 

# This is what we expect the ADAGE output file to be named.
PREFIX_RM="all-pseudomonas-gene-normalized_300_batch10_epoch500_corrupt0.1_lr0.01_seed1_"

# Output to a directory named 'pw_coverage_individual'
OUTPUT_DIR="./pw_coverage_individual"
mkdir -p $OUTPUT_DIR
OUTPUT_FILE_PREFIX="net300_ADAGE_sigPathway_model_" 

for f in $CURR_FOLDER/*.txt
do 
	echo "Processing $f file..."
	model_num=${f#$CURR_FOLDER/$PREFIX_RM}
	model_num=${model_num%%_*}  # gets rid of string after the model number.
	output_file="$OUTPUT_DIR/$OUTPUT_FILE_PREFIX"
	output_file+="$model_num"
	output_file+=".txt"
	Rscript "$RUN_SCRIPT" "$f" "$COMPENDIUM" "$KEGG_LIST" "$output_file"
done
